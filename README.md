# platzi_vr_final

Resultado final del curso de VR con UE4 de Platzi luego de hacerle las modificaciones del reto.

Este proyecto usa algunos elementos gratuitos del marketplace de Unreal Engine, como por ejemplo 
los escenarios de Infinity Blade: GrassLands. Estos assets pueden ser usados comercialmente en 
tus proyectos pero siguen funcionando bajo la licencia de EULA de Unreal Engine. 
Para mas información lee este post: 
https://www.unrealengine.com/en-US/blog/free-infinity-blade-collection-marketplace-release



